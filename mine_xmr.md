[TOC]

# Linux

## Linux 命令

```bash
 # 打开terminal时的motd的修改(Message of the day)
 https://linuxconfig.org/how-to-change-welcome-message-motd-on-ubuntu-18-04-server
 # 输出当前系统信息
 landscape-sysifno
 # 输出当前主机的所有内外网ip
 hostname -i
```











## Linux工具



### thefuck 自动纠正前一个命令的错误

```bash
https://github.com/nvbn/thefuck
```



### alias

```bash
# 定义快捷命令
alias cd_myfile="cd /home/myfile" # 将这句话加入.zshrc或者.bashrc，就可以永久生效
```





### Vim Vi

```bash
# vi显示代码时没有色彩
	是因为配置文件没有定义，安装一下vim就好了
	
# vim编辑时，xshell中鼠标不能弹出右键菜单，
	找到~/.vimrc（如果没有手动创建vim ~/.vimrc）将set mouse=a这一项改成set mouse=c，如果没有添加即可，

# 中文乱码问题，
    在.vimrc文件中添加set enc=utf8

```



### 7z

```bash
apt-cache search 7z
apt-get install p7zip-full
7z e xxx.7z
```

### Rclone

```bash
apt install rclone
rclone cofnig # 按照提示来
# 假设rclone config中为配置的网盘取名为ct
rclone copy xxx ct:folder # 拷贝文件到网盘的folder文件夹下
```





### BaiduPCS-Go

```bash
wget https://github.com/iikira/BaiduPCS-Go/releases/download/v3.5.6/BaiduPCS-Go-v3.5.6-linux-386.zip && unzip BaiduPCS-Go-v3.5.6-linux-386.zip
### https://github.com/iikira/BaiduPCS-Go
cp BaiduPCS-go /usr/bin/baidupcs-go && chmod +x /usr/bin/baidupcs-go # 就可以直接执行了
```



### 小鸡FQ必备

```bash
# brook安装
	wget https://github.com/ToyoDAdoubi/doubi/raw/master/brook.sh && bash brook.sh
	
# v2ray安装
	bash <(curl -L https://github.com/233boy/v2ray/raw/master/install.sh)
	
# aria2c安装
	http://sesprie.bid/articles/37.html
	
# nextcloud安装


# tmux的安装和使用
    sudo apt install tmux -y
    yum install tmux -y 
    创建会话：
    tmux new -s mysession
    进入session:tmux a -t mysession
    脱离session：Ctrl+b d
    竖分屏Ctrl+b %
    横分屏Ctrl+b "
    
# oh-my-zsh
    apt install zsh -y
    via curl：
    bash -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    via wget：
    bash -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

# 测速脚本superbench
    https://www.oldking.net/350.html 
    wget -qO- git.io/superbench.sh | bash

# autojump
    git clone git://github.com/wting/autojump.git
    cd autojump
    ./install.py or ./uninstall.py
    
# alias
	alias永久生效需要修改.bashrc或者.zshrc文件，加上例如alias ch="cd ~"这样的就可以了alias
	
# rclone
	rclone copy folder ct:folder2 # 这个命令会将folder内的所有内容复制到挂载的ct网盘的folder2目录内，且保留目录结构
	
# 
```



## Vim常用按键

```bash
u			撤销上一步的操作
Ctrl+r 		恢复上一步被撤销的操作
^			移动光标到行首
$			移动光标到行尾  （就是正则表达式里面的首部和尾部的符号）
Esc			切换到命令模式
:wq			退出并保存
vim查找替换：
#查找替换的格式
:范围/查找的内容/替换后的内容
# 将所有的`},`替换成`},换行`，
:%s/\},/\},\r/g # 在vim中\r是换行，\n会被替换成@

# 查找字符串，word即要查找的词
:/word

参考：https://www.jianshu.com/p/3abd6fbc3322
```



## Monero挖矿信息

```bash
pool.minexmr.com:4444
435tinNC6FiHNDJg4GTbZW2qr8ak2xV7oAdj1wdXvd2kCH7GrAVV2Q2SjR1gz4Rc99FPUgNGiqacXS3tH3WsLsxLNNyHS9o


nanopool矿池，xmr-stak-bin-2.5.1，xmr-asia1.nanopool.org:14444
https://xmr.nanopool.org/help#poolsettings

```



## Linux小命令

```bash
# 返回上一次的目录
cd -


```

## 关于定时任务的排查

```bash
http://www.voidcn.com/article/p-nwzquqrk-bmu.html
```







## Android

### Termux + Termux Styling

```bash
下载termux和termux styling
下面的是配置
# 获取虚拟root权限
pkg install proot && termux-chroot

# 追加源
echo "deb [trusted=yes] http://termux.iikira.com stable main \n# The termux repository mirror from TUNA:\ndeb [arch=all,arm] https://mirrors.tuna.tsinghua.edu.cn/termux stable main" >> /data/data/com.termux/files/usr/etc/apt/sources.list

# 一键配置界面（配置代理可能更快）
apt update -y && apt install wget zip git curl vim -y && sh -c "$(curl -fsSL https://github.com/Cabbagec/termux-ohmyzsh/raw/master/install.sh)" (配置后选择主题选择21，字体留空，否则更改之后，字体看起来很不爽)


https://tonybai.com/2017/11/09/hello-termux/
# 常用
pkg install curl wget unzip p7zip nmap vim git unrar

# 开启ftp服务
alias ftp1024="tcpsvd -vE 0.0.0.0 1024 ftpd -w /sdcard/"
https://wiki.termux.com/wiki/Remote_Access
```

[Termux - ohmyzsh](https://github.com/Cabbagec/termux-ohmyzsh)

[Termux高级终端安装使用配置教程](https://www.freebuf.com/geek/170510.html)

[JuiceSSH连接termux](https://www.sfantree.com/termux_01/)

[Xshell 密钥生成](https://www.linuxidc.com/Linux/2015-10/123862.htm) 跟juicessh连接termux一样的套路，需要再同一局域网下，需要找到手机的ip地址





### Neoterm

### BaiduPCS-Go

[Android运行baidupcs-go教程](https://github.com/iikira/BaiduPCS-Go/wiki/Android-%E8%BF%90%E8%A1%8C%E6%9C%AC%E9%A1%B9%E7%9B%AE%E7%A8%8B%E5%BA%8F%E5%8F%82%E8%80%83%E7%A4%BA%E4%BE%8B)

```bash
# 一键安装arm64的
apt install wget curl -y (重启termux)
cd && rm -rf BaiduPCS-Go-v3.5.6-android-21-arm64 && wget https://github.com/iikira/BaiduPCS-Go/releases/download/v3.5.6/BaiduPCS-Go-v3.5.6-android-21-arm64.zip && unzip BaiduPCS-Go-v3.5.6-android-21-arm64.zip && cd BaiduPCS-Go-v3.5.6-android-21-arm64 && chmod +x BaiduPCS-Go && mv BaiduPCS-Go baidupcs-go && mv baidupcs-go ../../usr/bin/ && cd && rm -rf BaiduPCS-Go-v3.5.6-android-21-arm64 && rm baidupcs-down && baidupcs-go config set -savedir="/sdcard/baidupcs-down" && ln -s /sdcard/baidupcs-down baidupcs-down && baidupcs-go set -user_agent="Mozilla;5.0" && baidupcs-go set -cache_size=250000 && baidupcs-go set -max-parallel=200

```















## Ubuntu

### Ubuntu16.04

#### Xmr-Stak 一键编译

```bash
    # cmake的选项中关闭了Gpu的选项，generic表示通用版本
    apt update -y &&
    apt install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev &&
    git clone https://github.com/fireice-uk/xmr-stak.git &&
    mkdir xmr-stak/build &&
    cd xmr-stak/build &&
    cmake .. -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF -DXMR-STAK_COMPILE=generic &&
    make install
    # 打包到~目录
    mkdir xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04-generic &&
    mv bin xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04-generic &&
    tar -zcvf xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04-generic.tar.gz xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04-generic &&
    mv xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04-generic.tar.gz ~
    
```



#### Xmr-Stak 一键下载执行

```bash
    echo -e "nameserver 8.8.8.8" > /etc/resolv.conf &&
    apt update -y &&
    apt install python3 -y &&
    apt install screenfetch -y &&
    apt install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev -y &&
    wget https://github.com/Sesprie/temp/raw/master/xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04.tar.gz &&
    tar -zxvf xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04.tar.gz &&
    cd xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04/bin &&
    chmod +x xmr-stak &&
    mv xmr-stak myapp &&
    wget https://github.com/Sesprie/temp/raw/master/start.py &&
    nohup python3 start.py &
    
    # ---------------generic版本----------------------------------------------------
    
	echo -e "nameserver 8.8.8.8" > /etc/resolv.conf &&
    apt update -y &&
    apt install python3 -y &&
    apt install screenfetch -y &&
    apt install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev -y &&
    wget https://github.com/Sesprie/temp/raw/master/xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04-generic.tar.gz &&
    tar -zxvf xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04-generic.tar.gz &&
    cd xmr-stak-2.5.1-bin-minexmrPool-ubuntu16.04-generic/bin &&
    chmod +x xmr-stak &&
    mv xmr-stak myapp &&
    wget https://github.com/Sesprie/temp/raw/master/start.py &&
    nohup python3 start.py &
    
    # 如果运行xmr-stak，出现Illegal instruction (core dumped)，应该是编译环境的原因，需要重新编译，在cmake .. 选项中加入-DXMR-STAK_COMPILE=generic选项，系统环境不尽相同，这可能就是xmr-stak没有linux系统下编译好的二进制文件的原因
```



### Ubuntu18.04

#### Xmr-Stak 一键编译

```bash
# Ubuntu 18.04 xmr-stak 一键编译执行
pool.minexmr.com:4444
435tinNC6FiHNDJg4GTbZW2qr8ak2xV7oAdj1wdXvd2kCH7GrAVV2Q2SjR1gz4Rc99FPUgNGiqacXS3tH3WsLsxLNNyHS9o
# 参考页面
# https://github.com/fireice-uk/xmr-stak/blob/master/doc/compile_Linux.md
# https://bitcointalk.org/index.php?topic=304389.0
# 没有Gpu组件时需要加上选项 cmake .. -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF




echo -e "nameserver 8.8.8.8" > /etc/resolv.conf &&
sudo apt install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev -y &&
git clone https://github.com/fireice-uk/xmr-stak.git &&
mkdir xmr-stak/build &&
cd xmr-stak/build &&
free &&
dd if=/dev/zero of=/var/swap.img bs=1024k count=1000 &&
mkswap /var/swap.img &&
swapon /var/swap.img &&
free &&
cmake .. -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF &&
make install &&
swapoff /var/swap.img &&
rm -rf /var/swap.img &&
cd bin &&
./xmr-stak




# 关闭刚才创建的交换分区
swapoff /var/swap.img

更多关于交换分区，以及dd命令
https://www.jianshu.com/p/28487f0b0dd8
http://man.linuxde.net/mkswap
http://man.linuxde.net/dd
https://stackoverflow.com/questions/30887143/make-j-8-g-internal-compiler-error-killed-program-cc1plus

```



#### Xmr-Stak 一键下载执行

```bash


# Ubuntu 18.04 xmr-stak 一键下载执行
# 查看本机ip地址
# hostname -I

echo -e "nameserver 8.8.8.8" > /etc/resolv.conf &&
apt-get update -y &&
apt-get install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev -y &&
apt-get install wget -y &&
wget https://github.com/Sesprie/temp/raw/master/xmr-stak-2.5.1-bin-minexmrPool-ubuntu18.04.tar.gz &&
tar -zxvf xmr-stak-2.5.1-bin-minexmrPool-ubuntu18.04.tar.gz &&
cd xmr-stak-2.5.1-bin-minexmrPool-ubuntu18.04/bin &&
chmod +x xmr-stak &&
hostname -I &&
mv xmr-stak myapp &&
wget https://github.com/Sesprie/temp/raw/master/start.py &&
nohup python3 start.py &


```



### Docker Nextcloud的安装

```bash

apt-get update -y &&
apt-get install docker.io -y &&
systemctl start docker &&
docker pull nextcloud &&
docker run -d -p 8081:80 --restart=always --name mycloud nextcloud &&
curl 0.0.0.0:8081

# --restart=always表示开机自启

netstat -a | grep 80
# 查询端口是否被占用
lsof -i:80
lsof -i:端口号



# 停止并删除所有容器
docker stop $(docker ps -q) && docker rm $(docker ps -aq)

# 查看端口的使用的情况
lsof 命令

# 比如查看80端口的使用的情况。
lsof -i tcp:80

# 列出所有的端口
netstat -ntlp

# 查看端口的状态
 /etc/init.d/iptables status

# 查看正在运行的程序
top
# 强制kill指定pid，pid可在top命令列出的列表中查看
kill -9 pid 
```



### 杀掉Command中含有指定关键字的进程

```bash
    # 杀掉所有command中含有apache2的所有进程
    ps -ef | grep apache2 | grep -v grep|cut -c 9-15|xargs kill -9
    # 杀掉指定关键字的进程
    pkill 进程名
    # 列出所有进程
    ps -ef
```

### 开启Vnc服务

```bash

ubuntu安装vnc
apt update -y && apt install xfce4 xfce4-goodies tightvncserver xfonts-base -y
# 启动vncserver配置密码，然后会提示设置和确认密码
vncserver 
此后的一律No

# 配置之前结束vnc服务
vncserver -kill :1 

mv ~/.vnc/xstartup ~/.vnc/xstartup.bak && vi ~/.vnc/xstartup

# 有时间折腾echo直接写入文件，但是shell会执行#!/bin/bash，如果加上-E，换行符\n又不会执行了

#!/bin/bash
xrdb $HOME/.Xresources
startxfce4 &

# 配置启动权限
chmod +x ~/.vnc/xstartup

# 启动服务，提示中的":数字"，数字表示的是端口号
vncserver

下载vncviewer
ip:port
运行vncviewer时会有一个:1或者其他的数字，这个数字就是端口号

# 关闭ubuntu防火墙
ufw disable
```

### 安装ping

```bash
    apt-get install -y iputils-ping
```

### 搜索Nvidia驱动

```bash
    apt-cache search nvidia-*
    apt-get install nvidia-384 -y 
    # 搜索是否已安装驱动
    lsmod | grep nvidia
```

### 一键安装Screenfetch

```bash
    apt-get update -y && apt-get install screenfetch -y && screenfetch
```

### DigitalOcean小鸡密码

```bash
91tLbFIfPP7n
```





### 一键安装V2ray服务器端

```bash
	# v2ray不区分服务端和客户端，用配置文件来区分
	bash <(curl -s -L https://github.com/Sesprie/v2ray/raw/master/install.sh)
```



## Centos

### Xmr-Stak一键编译

```bash
# 不同的发行版本对应不懂的编译依赖
# 如果所有的依赖都已安装，但运行时提示某个依赖不存在，可能是依赖组件的版本不同，需要在当前环境环境下重新编译
sudo apt install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev
git clone https://github.com/fireice-uk/xmr-stak.git
mkdir xmr-stak/build
cd xmr-stak/build
cmake ..
make install
参开该页面编译，如果没有gpu，会提示错误，错误中会让加入一些选项，加载编译的后面即可
cmake ..  -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF
https://github.com/fireice-uk/xmr-stak/blob/master/doc/compile_Linux.md



centos编译版本，centos6以后openssl替代了libssl
yum install centos-release-scl epel-release cmake3 devtoolset-4-gcc* hwloc-devel libmicrohttpd-devel openssl-devel make -y &&
scl enable devtoolset-4 bash
git clone https://github.com/fireice-uk/xmr-stak.git && 
mkdir xmr-stak/build &&
cd xmr-stak/build && 
cmake3 .. -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF &&
make install

遇到no package available的情况，查找对应centos版本的aliyun源即可
几经折腾，最终放弃
c++: internal compiler error: Killed (program cc1plus)
Please submit a full bug report,
with preprocessed source if appropriate.
See <http://bugzilla.redhat.com/bugzilla> for instructions.
make[2]: *** [CMakeFiles/xmr-stak-backend.dir/xmrstak/backend/cpu/minethd.cpp.o] Error 4
make[1]: *** [CMakeFiles/xmr-stak-backend.dir/all] Error 2
make: *** [all] Error 2

# 貌似是交换空间不够
I had the same problem ona  digital ocean $5 VPS. Couldn't compile a client to save my life. The fix was to do the following:

Code:
# 依次键入，再次编译安装即可
free
dd if=/dev/zero of=/var/swap.img bs=1024k count=1000
mkswap /var/swap.img
swapon /var/swap.img
free
make -f makefile.unix

Regardless of what the first poster said. Swap will overcome this problem.


```

### Xmr-Stak一键下载执行

```bash
apt-get install -y tmux && tmux && sudo apt-get install-y git libcurl4-openssl-dev build-essential libjansson-dev autotools-dev automake && git clone https://github.com/hyc/cpuminer-multi
cd cpuminer-multi && ./autogen.sh && CFLAGS="-march=native" ./configure && make


wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_8.0.61-1_amd64.deb && sudo dpkg -i cuda-repo-ubuntu1604_8.0.61-1_amd64.deb &&
 sudo apt update && 
 sudo apt install cuda-8-0 -y


## If you want to keep the graphical interface start at boot ignore from
## HERE
sudo vi /etc/default/grub
----
Comment :
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
----
sudo update-grub
sudo systemctl disable lightdm.service
## TO HERE

sudo usermod -a -G video mining

sudo reboot



修改dns,
echo -e "nameserver 8.8.8.8" > /etc/resolv.conf
开始挖mnoero，xmrig
apt-get install tmux -y && tmux 
!wget  https://github.com/Sesprie/temp/raw/master/xmrig-2.8.3-amd64.tar.gz && tar -zxvf xmrig-2.8.3-amd64.tar.gz && cd xmrig-2.8.3 && ./xmrig

```



### 一键安装Screenfetch

```bash
# centos 安装screenfetch
sudo yum install git &&
git clone git://github.com/KittyKatt/screenFetch.git screenfetch &&
sudo cp screenfetch/screenfetch-dev /usr/bin/screenfetch &&
sudo chmod +x /usr/bin/screenfetch &&
screenfetch

https://tanmaync.wordpress.com/2017/10/02/install-screenfetch-centos7-rhel7/
```



