apt update -y
apt install wget
cd 
url="https://github.com/iikira/BaiduPCS-Go/releases/download/v3.5.6/BaiduPCS-Go-v3.5.6-android-21-arm64.zip"
archive_name=${url##*/}  # 截取从左向右最后一个/后面的文本
dir_name=${archive_name%.*}  # 截取从右向左的第一个.后面的文本

if [[ ! -f $HOME/${archive_name} ]]; then
  echo -e "\033[32m开始下载${url}...\033[0m"
  wget ${url}
fi
unzip $HOME/${archive_name}
cd $HOME/${dir_name}
mv BaiduPCS-Go baiduPCS-Go
chmod 777 baiduPCS-Go
mv baiduPCS-Go $PREFIX/bin 
cd
baiduPCS-Go config set -cache_size 260000 -max_parallel 500 -max_download_load 5 -enable_https -appid 265486
# rm -rf $HOME/${archive_name} $HOME/${dir_name} # 这种操作还是不要了，错将dir_name写成dir，导致直接将$HOME目录删除了。。。呜呜呜。。。。
# rm -rf $HOME/tmp


