#!/bin/bash
current=`date +%F-%T`

function echo_green() {
        echo -e "\033[32m[ $1 ]\033[0m"
}

for i in "$@"; do
        echo_green "$i"
        mkdir -p /trash/${current}
        mv -i "$i" /trash/${current}/
done
