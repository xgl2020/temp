#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

#=================================================
#	
#
#      功能：在CentOS/Debian/Ubuntu上安装Tcping工具
#      时间：2019/10/1
#
#=================================================



Green_font_prefix="\033[32m" && Red_font_prefix="\033[31m" && Green_background_prefix="\033[42;37m" && Red_background_prefix="\033[41;37m" && Font_color_suffix="\033[0m"
Info="${Green_font_prefix}[信息]${Font_color_suffix}"
Error="${Red_font_prefix}[错误]${Font_color_suffix}"
Tip="${Green_font_prefix}[注意]${Font_color_suffix}"


# 检查当前是否具有root权限
check_root(){
	[[ $EUID != 0 ]] && echo -e "${Error} 当前非ROOT账号(或没有ROOT权限)，无法继续操作，请更换ROOT账号或使用 ${Green_background_prefix}sudo su${Font_color_suffix} 命令获取临时ROOT权限（执行后可能会提示输入当前账号的密码）。" && exit 1
}


#检查系统
check_sys(){
	if [[ -f /etc/redhat-release ]]; then
		release="centos"
	elif cat /etc/issue | grep -q -E -i "debian"; then
		release="debian"
	elif cat /etc/issue | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
	elif cat /proc/version | grep -q -E -i "debian"; then
		release="debian"
	elif cat /proc/version | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
    fi
	bit=`uname -m`
}




Installation_dependency(){
	if [[ ${release} = "centos" ]]; then
		yum update -y
		yum install -y tcptraceroute bc
	else
		apt-get update -y
		apt-get install -y tcptraceroute bc
	fi
}


install_tcping(){
	# https://soft.mengclaw.com/Bash/TCP-PING
	wget -O /usr/bin/tcping https://github.com/Sesprie/temp/raw/master/soft/tcping/bin/linux/tcping
	chmod +x /usr/bin/tcping
	echo -e "${Green_font_prefix}安装成功！${Font_color_suffix}"
}

main(){
	check_root
	check_sys
	Installation_dependency
	install_tcping
}



main



echo -e "使用方法：tcping google.com 443\n"




echo -e "Tips: 为保证将代码粘贴到vps中的文件中时，格式不乱，建议使用${Green_font_prefix} nano ${Font_color_suffix}打开文件再粘贴"


# 参考：
# https://www.mengclaw.com/2018/06/03/184/
# https://web.archive.org/web/20191001074204/https://www.mengclaw.com/2018/06/03/184/
# https://archive.li/0xuWF
# 
# https://blog.kelu.org/tech/2017/11/04/TCPing-for-linux.html
# https://web.archive.org/web/20191001075849/https://blog.kelu.org/tech/2017/11/04/TCPing-for-linux.html
# 
# https://www.xuxusheng.com/post/linux%E5%B0%8F%E5%B7%A5%E5%85%B7%E4%B9%8B-tcping
# 
# windows版
# https://web.archive.org/web/20190924172803/https://elifulkerson.com/projects/tcping.php



# tcping二进制文件备份：
# wget -O tcping https://cdn.kelu.org/blog/2017/11/tcpping
# wget -O tcping https://soft.mengclaw.com/Bash/TCP-PING
